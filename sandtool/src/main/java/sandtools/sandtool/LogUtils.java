//   Copyright 2014, 2015 Thilo Planz
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

package sandtools.sandtool;

import static org.apache.commons.lang3.time.DurationFormatUtils.formatDurationHMS;

import java.io.File;
import java.io.IOException;

import org.apache.commons.lang3.time.StopWatch;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.encoder.PatternLayoutEncoder;
import ch.qos.logback.classic.filter.ThresholdFilter;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.ConsoleAppender;
import ch.qos.logback.core.FileAppender;
import ch.qos.logback.core.OutputStreamAppender;

public class LogUtils {

    public static Logger LOGGER = LoggerFactory.getLogger("Sandtool");

    public static StopWatch startWatch() {
        StopWatch s = new StopWatch();
        s.start();
        return s;
    }

    public static void lapTime(long millis, String logMessage, Object... args) {
        if (LOGGER.isInfoEnabled()) {
            LOGGER.info("[.... " + formatDurationHMS(millis) + "] " + logMessage, args);
        }
    }

    /**
     * Stops the stopWatch and logs a message as INFO
     */

    public static StopWatch stopWatch(StopWatch s, String logMessage, Object... args) {
        s.stop();
        if (LOGGER.isInfoEnabled()) {
            LOGGER.info("[took " + s.toString() + "] " + logMessage, args);
        }
        return s;
    }

    /**
     * Stops the stopWatch and logs a message as DEBUG (or at a higher level if
     * the task took quite long)
     */
    public static StopWatch stopWatchDebug(StopWatch s, String logMessage, Object... args) {
        s.stop();
        long taken = s.getTime();
        if (taken < 1000) {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("[took " + s.toString() + "] " + logMessage, args);
            }
            return s;
        }
        LOGGER.info("[took " + s.toString() + "] " + logMessage, args);
        return s;
    }

    /**
     * Stops the stopWatch and logs a message as TRACE (or at a higher level if
     * the task took quite long)
     */
    public static StopWatch stopWatchTrace(StopWatch s, String logMessage, Object... args) {
        s.stop();
        long taken = s.getTime();
        if (taken < 50) {
            if (LOGGER.isTraceEnabled()) {
                LOGGER.trace("[took " + s.toString() + "] " + logMessage, args);
            }
            return s;
        }
        if (taken < 1000) {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("[took " + s.toString() + "] " + logMessage, args);
            }
            return s;
        }
        LOGGER.info("[took " + s.toString() + "] " + logMessage, args);
        return s;
    }

    private static ch.qos.logback.classic.Logger ensureLevel(ch.qos.logback.classic.Logger logger, Level level) {
        Level l = logger.getLevel();
        if (l == null || l.isGreaterOrEqual(level)) {
            logger.setLevel(level);
        }
        return logger;
    }

    static void configureToolOutput(Level sandtool, Level others) {
        try {
            LoggerContext context = (LoggerContext) LoggerFactory.getILoggerFactory();

            // log to System.out
            {
                ConsoleAppender<ILoggingEvent> ca = new ConsoleAppender<ILoggingEvent>();

                ch.qos.logback.classic.Logger logger = ensureLevel(context.getLogger("Sandtool"), sandtool);

                ThresholdFilter f = new ThresholdFilter();
                f.setLevel(sandtool.toString());
                f.start();
                ca.addFilter(f);

                ca.setContext(context);
                ca.setName("Sandtool");

                PatternLayoutEncoder pl = new PatternLayoutEncoder();
                pl.setContext(context);
                pl.setPattern("%msg%n");
                pl.start();

                ca.setEncoder(pl);
                ca.start();

                logger.addAppender(ca);
                logger.setLevel(sandtool);
                logger.setAdditive(false);
            }

            // also enable logging for other module, but filtered a bit more
            {

                ch.qos.logback.classic.Logger rootLogger = ensureLevel(context.getLogger(Logger.ROOT_LOGGER_NAME),
                        others);

                ConsoleAppender<ILoggingEvent> ca = new ConsoleAppender<ILoggingEvent>();
                ca.setContext(context);
                ca.setName("Sandtool libraries");

                ThresholdFilter f = new ThresholdFilter();
                f.setLevel(others.toString());
                f.start();
                ca.addFilter(f);

                PatternLayoutEncoder pl = new PatternLayoutEncoder();
                pl.setContext(context);
                pl.setPattern("[%-5level] %logger{36} - %msg%n");
                pl.start();

                ca.setEncoder(pl);
                ca.start();

                rootLogger.addAppender(ca);
            }

        } catch (Exception e) {
            LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME).error("failed to set up logging", e);
        }
    }

    static void setupErrorLogging() {
        try {
            LoggerContext context = (LoggerContext) LoggerFactory.getILoggerFactory();
            context.reset();

            // log ERRORS to System.err
            ConsoleAppender<ILoggingEvent> ca = new ConsoleAppender<ILoggingEvent>();
            ca.setTarget("System.err");
            ca.setContext(context);
            ca.setName("STDERR");
            ThresholdFilter onlyErrors = new ThresholdFilter();
            onlyErrors.setLevel("WARN");
            onlyErrors.start();
            ca.addFilter(onlyErrors);

            PatternLayoutEncoder pl = new PatternLayoutEncoder();
            pl.setContext(context);
            pl.setPattern("%d [%thread] %-5level %logger{36} - %msg%n");
            pl.start();

            ca.setEncoder(pl);
            ca.start();

            ch.qos.logback.classic.Logger rootLogger = context.getLogger(Logger.ROOT_LOGGER_NAME);
            rootLogger.addAppender(ca);
            rootLogger.setLevel(Level.ERROR);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    static void configureLogFile(Level sandtool, Level others, String filename) throws IOException {

        OutputStreamAppender<ILoggingEvent> app;

        // allow for filename "-" to mean STDOUT
        if ("-".equals(filename)) {
            app = new ConsoleAppender<ILoggingEvent>();
        } else {
            File f = new File(filename);
            if (!f.exists()) {
                f.createNewFile();
            }
            if (!f.isFile() || !f.canWrite()) {
                throw new IllegalArgumentException("cannot write to logfile " + f.getAbsolutePath());
            }
            FileAppender<ILoggingEvent> fapp = new FileAppender<>();
            fapp.setAppend(true);
            fapp.setFile(filename);
            app = fapp;
        }
        LoggerContext context = (LoggerContext) LoggerFactory.getILoggerFactory();

        app.setContext(context);

        PatternLayoutEncoder pl = new PatternLayoutEncoder();
        pl.setContext(context);
        pl.setPattern("%d [%thread] %-5level %logger{36} - %msg%n");
        pl.start();

        app.setEncoder(pl);
        app.start();

        ch.qos.logback.classic.Logger rootLogger = ensureLevel(context.getLogger(Logger.ROOT_LOGGER_NAME), others);
        rootLogger.addAppender(app);

        ch.qos.logback.classic.Logger toolLogger = ensureLevel(context.getLogger("Sandtool"), sandtool);
        toolLogger.addAppender(app);
        toolLogger.setAdditive(false);
    }

}
