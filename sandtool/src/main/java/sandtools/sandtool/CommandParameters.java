//   Copyright 2014, 2015 Thilo Planz
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

package sandtools.sandtool;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.commons.io.Charsets;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

import ch.qos.logback.classic.Level;

public class CommandParameters {

    private final List<String> arguments = new ArrayList<>();

    private Properties parameters = new Properties();

    private final String command;

    CommandParameters(String[] argv) throws IOException {

        // we allow the "-" (but not the "--") arguments to appear before the
        // command name,
        // so first find out where the command actually is
        int commandIdx = -1;
        for (int i = 0; i < argv.length; i++) {
            String arg = argv[i];
            if (!arg.startsWith("-")) {
                commandIdx = i;
                break;
            }
            if (arg.startsWith("--")) {
                throw new IllegalArgumentException("parameter " + arg + " must come after the command name");
            }
            // some flags take an extra file name parameter
            if (arg.startsWith("-L") || "-f".equals(arg)) {
                i++;
            }
        }

        if (commandIdx == -1)
            throw new IllegalArgumentException("missing command name");

        command = argv[commandIdx];

        // load the command definition file (can be null, but then there must be
        // a command definition in the other options that will be passed in)
        InputStream in = getClass().getResourceAsStream(command + ".command");
        if (in != null)
            try {
                parameters.load(in);
            } finally {
                in.close();
            }

        for (int i = 0; i < argv.length; i++) {
            if (i == commandIdx)
                continue;
            String arg = argv[i];

            // only arguments starting with "-" are treated as parameters
            if (!arg.startsWith("-")) {
                arguments.add(arg);
                continue;
            }

            // --some.option foo
            if (arg.startsWith("--")) {
                if (++i >= argv.length)
                    throw new IllegalArgumentException("missing value for parameter " + arg);
                String value = argv[i];
                parameters.put(arg.substring(2), value);
                continue;
            }

            switch (arg) {
            case "-":
                // read configuration file from standard input
                parameters = parseConfigurationFile(IOUtils.toString(System.in, Charsets.UTF_8), parameters);
                continue;

            case "-f":
                // read configuration file
                if (++i >= argv.length)
                    throw new IllegalArgumentException("missing configuration file name after " + arg);
                // allow "-f -" as a (probably preferred) version of "-"
                if ("-".equals(argv[i])) {
                    parameters = parseConfigurationFile(IOUtils.toString(System.in, Charsets.UTF_8), parameters);
                    continue;
                }
                File f = new File(argv[i]);
                if (!f.canRead()) {
                    throw new IllegalArgumentException("configuration file " + f.getAbsolutePath() + " cannot be read");
                }
                parameters = parseConfigurationFile(FileUtils.readFileToString(f, Charsets.UTF_8), parameters);
                continue;

            case "-v":
                LogUtils.configureToolOutput(Level.INFO, Level.WARN);
                continue;

            case "-vv":
                LogUtils.configureToolOutput(Level.DEBUG, Level.INFO);
                continue;

            case "-vvv":
                LogUtils.configureToolOutput(Level.TRACE, Level.DEBUG);
                continue;

            case "-L":
                if (++i >= argv.length)
                    throw new IllegalArgumentException("missing logfile name after " + arg);
                LogUtils.configureLogFile(Level.INFO, Level.INFO, argv[i]);
                continue;

            case "-Lv":
                if (++i >= argv.length)
                    throw new IllegalArgumentException("missing logfile name after " + arg);
                LogUtils.configureLogFile(Level.DEBUG, Level.INFO, argv[i]);
                continue;

            case "-Lvv":
                if (++i >= argv.length)
                    throw new IllegalArgumentException("missing logfile name after " + arg);
                LogUtils.configureLogFile(Level.DEBUG, Level.DEBUG, argv[i]);
                continue;

            case "-Lvvv":
                if (++i >= argv.length)
                    throw new IllegalArgumentException("missing logfile name after " + arg);
                LogUtils.configureLogFile(Level.TRACE, Level.DEBUG, argv[i]);
                continue;

            default:
                throw new IllegalArgumentException("unsupported parameter " + arg);
            }

        }

        // check if the command class is defined now
        if (StringUtils.isBlank(parameters.getProperty("sandtool.command." + command + ".class")))
            throw new IllegalArgumentException("unknown command " + command);

    }

    public Properties getParameters() {
        return parameters;
    }

    public String[] getArguments() {
        return arguments.toArray(new String[arguments.size()]);
    }

    public String getRequiredString(String key) {
        String x = StringUtils.stripToNull(parameters.getProperty(key));

        if (x == null)
            throw new IllegalArgumentException("missing required parameter " + key);
        return x;
    }

    public boolean getRequiredBoolean(String key) {
        String x = getRequiredString(key);
        if ("yes".equalsIgnoreCase(x) || "y".equalsIgnoreCase(x) || "true".equalsIgnoreCase(x)
                || "on".equalsIgnoreCase(x))
            return true;
        if ("no".equalsIgnoreCase(x) || "n".equalsIgnoreCase(x) || "false".equalsIgnoreCase(x)
                || "off".equalsIgnoreCase(x))
            return false;
        throw new IllegalArgumentException("invalid value for boolean " + key + ": " + x);
    }

    public <T extends Enum<T>> List<T> getRequiredActionList(@SuppressWarnings("unchecked") T... options) {
        List<T> selected = new ArrayList<>();

        strings: for (String s : arguments) {
            for (T option : options) {
                if (option.name().equals(s)) {
                    selected.add(option);
                    continue strings;
                }
            }
            throw new IllegalArgumentException("invalid action " + s);
        }
        return selected;
    }

    public <T extends Enum<T>> T getRequiredOption(Class<T> enumType, String key) {
        return Enum.valueOf(enumType, getRequiredString(key));
    }

    public File getOptionalOutputFile(String key) {
        String x = StringUtils.stripToNull(parameters.getProperty(key));
        if (x == null)
            return null;
        File f = new File(x);
        if (f.isDirectory())
            throw new IllegalArgumentException(key + "(" + f.getAbsolutePath()
                    + ")  is not a valid output file, it is a directory");
        if (f.exists() && !f.canWrite())
            throw new IllegalArgumentException(key + "(" + f.getAbsolutePath() + ")  is not writable");
        return f;
    }

    public File getOptionalDirectory(String key) {
        String x = StringUtils.stripToNull(parameters.getProperty(key));
        if (x == null)
            return null;
        File f = new File(x);
        if (!f.exists())
            return f;
        if (!f.isDirectory())
            throw new IllegalArgumentException(key + "(" + f.getAbsolutePath() + ")  is not a directory");
        return f;
    }

    Command getCommand() throws InstantiationException, IllegalAccessException, ClassNotFoundException {
        return Class.forName(getRequiredString("sandtool.command." + command + ".class")).asSubclass(Command.class)
                .newInstance();
    }

    String getCommandName() {
        return command;
    }

    public static Properties parseConfigurationFile(String data, Properties defaults) throws IOException {
        Properties p = new Properties(defaults);

        // see if we have the --- divider
        int divider = data.indexOf("---");

        if (divider == 0) {
            // at the start of the file
            addConfigurationSections(p, data);
            return p;
        }

        // if not at the start of the file, must be at the start of the line
        divider = data.indexOf("\n---", divider - 1);

        if (divider == -1) {
            p.load(new StringReader(data));
        } else {
            p.load(new StringReader(data.substring(0, divider)));
            addConfigurationSections(p, data.substring(divider + 1));
        }
        return p;
    }

    private static void addConfigurationSections(Properties p, String data) {
        String line = StringUtils.substringBefore(data, "\n");
        String rest = data.substring(line.length() + 1);
        if (line.startsWith("---'")) {
            String key = StringUtils.substringBetween(line, "'");
            if (StringUtils.isBlank(key)) {
                throw new IllegalArgumentException("unsupported configuration section " + line);
            }
            p.put(key, rest);
            return;
        }
        throw new IllegalArgumentException("unsupported configuration section " + line);
    }

    public int getRequiredInteger(String key, int minValue, int maxValue) {
        String s = getRequiredString(key);
        try {
            int c = Integer.parseInt(s);
            if (c < minValue || c > maxValue)
                throw new IllegalArgumentException(key + " has to be an integer between " + minValue + " and "
                        + maxValue);
            return c;
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException(key + " has to be an integer between " + minValue + " and " + maxValue);
        }
    }

}
