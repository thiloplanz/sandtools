//   Copyright 2014, 2015 Thilo Planz
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

package sandtools.sandtool;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.StopWatch;

public class Main {

    public static void main(String[] args) {

        LogUtils.setupErrorLogging();

        CommandParameters params;

        try {
            params = new CommandParameters(args);
        } catch (Exception e) {
            LogUtils.LOGGER.error(e.toString());
            System.err.println(e);
            System.exit(1);
            return;
        }

        try {
            StopWatch s = LogUtils.LOGGER.isInfoEnabled() ? LogUtils.startWatch() : null;

            Command c = params.getCommand();
            int statusCode = c.main(params);
            if (s != null)
                LogUtils.stopWatch(s, "execute command '{} {}'", params.getCommandName(),
                        StringUtils.join(params.getArguments(), " "));
            System.exit(statusCode);
        } catch (Throwable e) {
            LogUtils.LOGGER.error("Command execution failed for '{} {}'", params.getCommandName(),
                    StringUtils.join(params.getArguments(), " "), e);
            System.err.println(e);
            System.exit(1);
        }
    }

}
