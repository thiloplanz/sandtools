//   Copyright 2015 Thilo Planz
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

package sandtools.reports;

import static java.math.BigDecimal.ZERO;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TabularData<R> {

    private final Map<String, ColumnDataSource<R, ?>> columnData = new HashMap<>();

    public <T> void setColumnDataSource(String columnKey, ColumnDataSource<R, ?> cds) {
        columnData.put(columnKey, cds);
    }

    public <V> void setColumnDataSource(String columnKey, Map<R, Map<String, V>> keyedMaps) {
        columnData.put(columnKey, mapEntryColumn(keyedMaps, columnKey));
    }

    public <V> void setNestedColumnDataSource(String columnKey, ColumnDataSource<R, Map<String, V>> baseColumn) {
        columnData.put(columnKey, nestedColumn(baseColumn, columnKey));
    }

    @SuppressWarnings("unchecked")
    public void setKeyColumn(String columnKey){
        columnData.put(columnKey, KEY_COLUMN);
    }
    
    public Object getValue(R rowId, String columnKey) {
        ColumnDataSource<R, ?> cds = columnData.get(columnKey);
        if (cds == null)
            return null;
        return cds.getValue(rowId);
    }

    ColumnDataSource<R, ?>[] columns(List<String> columnKeys) {
        @SuppressWarnings("unchecked")
        ColumnDataSource<R, ?>[] r = new ColumnDataSource[columnKeys.size()];

        int i = 0;
        for (String c : columnKeys) {
            r[i++] = columnData.get(c);
        }
        return r;
    }

    public BigDecimal columnTotal(Collection<? extends R> rowIds, String columnKey) {
        BigDecimal sum = null;
        for (R r : rowIds) {
            sum = sum(sum, getValue(r, columnKey));
        }
        return sum;
    }

    @SuppressWarnings("rawtypes")
    private final static ColumnDataSource KEY_COLUMN = new ColumnDataSource() {

        @Override
        public Object getValue(Object rowId) {
            return rowId;
        }
    };

    public <T, V> ColumnDataSource<R, V> mapEntryColumn(final Map<R, Map<T, V>> keyedMaps, final T columnKey) {

        return new ColumnDataSource<R, V>() {
            @Override
            public V getValue(R rowId) {
                Map<T, V> row = keyedMaps.get(rowId);
                if (row == null)
                    return null;
                return row.get(columnKey);
            }

        };
    }

    public <T, U, V> ColumnDataSource<R, V> nestedColumn(final ColumnDataSource<R, Map<U, V>> baseColumn,
            final U columnKey) {
        return new ColumnDataSource<R, V>() {

            @Override
            public V getValue(R rowId) {
                Map<U, V> map = baseColumn.getValue(rowId);
                if (map == null)
                    return null;
                return map.get(columnKey);
            }

        };
    }

    private static BigDecimal sum(BigDecimal a, Object x) {
        if (x == null)
            return a;
        if (x instanceof BigDecimal) {
            if (a == null) {
                return (BigDecimal) x;
            }
            return a.add((BigDecimal) x);
        }
        return sum(a, new BigDecimal(x.toString()));
    }

    public ColumnDataSource<R, BigDecimal> sumColumn(final String... columnIndexes) {
        return new ColumnDataSource<R, BigDecimal>() {

            @Override
            public BigDecimal getValue(R rowId) {
                BigDecimal sum = null;
                for (String idx : columnIndexes) {
                    sum = sum(sum, TabularData.this.getValue(rowId, idx));
                }
                return sum;
            }

        };
    }

    public ColumnDataSource<R, BigDecimal> differenceColumn(final String colA, final String colB) {
        return new ColumnDataSource<R, BigDecimal>() {

            @Override
            public BigDecimal getValue(R rowId) {
                BigDecimal a = sum(ZERO, TabularData.this.getValue(rowId, colA));
                Object b = TabularData.this.getValue(rowId, colB);
                if (b == null)
                    return a;
                return a.subtract(sum(ZERO, b));
            }

        };
    }

    public ColumnDataSource<R, Double> pctColumn(final String colA, final String colB) {
        return new ColumnDataSource<R, Double>() {

            @Override
            public Double getValue(R rowId) {
                Object a = TabularData.this.getValue(rowId, colA);
                if (a == null)
                    return null;
                Object b = TabularData.this.getValue(rowId, colB);
                if (b == null)
                    return null;
                double bb = Double.parseDouble(b.toString());
                if (bb == 0)
                    return null;
                return 100 * Double.parseDouble(a.toString()) / bb;
            }
        };
    }
}