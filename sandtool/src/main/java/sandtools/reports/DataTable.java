//   Copyright 2015 Thilo Planz
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

package sandtools.reports;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DataTable<R> {

    private final TabularData<R> data;

    private final Collection<R> rowIds;

    private final List<String> columnKeys = new ArrayList<>();

    private final Map<String, String> formatStrings = new HashMap<>();

    private final Map<String, String> defaultIfBlank = new HashMap<>();

    private final Map<String, String> defaultIfZero = new HashMap<>();

    public DataTable(TabularData<R> data, Collection<R> rowIds) {
        this.data = data;
        this.rowIds = rowIds;
    }

    public void setColumns(String... columnKeys) {
        this.columnKeys.clear();
        addColumns(columnKeys);
    }

    public void addColumns(String... columnKeys) {
        for (String c : columnKeys) {
            this.columnKeys.add(c);
        }
    }

    public DataTable<R> withColumns(String... columnKeys) {
        addColumns(columnKeys);
        return this;
    }

    private void putOrDelete(Map<String, String> m, String key, String x) {
        if (x == null)
            m.remove(key);
        else
            m.put(key, x);
    }

    public void setColumnFormat(String columnKey, String format, String defaultForBlank, String defaultForZero) {
        putOrDelete(formatStrings, columnKey, format);
        putOrDelete(defaultIfBlank, columnKey, defaultForBlank);
        putOrDelete(defaultIfZero, columnKey, defaultForZero);
    }

    public void setColumnFormat(String columnKey, String format) {
        setColumnFormat(columnKey, format, null, null);
    }

    /**
     * Fluent convenience method, adds a column and sets the format for it
     */
    public DataTable<R> withColumn(String columnKey, String format) {
        this.columnKeys.add(columnKey);
        setColumnFormat(columnKey, format);
        return this;
    }

    private final NumberFormat nf = NumberFormat.getIntegerInstance();
    {
        nf.setGroupingUsed(true);
        nf.setMaximumFractionDigits(2);
        nf.setMinimumFractionDigits(2);
    }

    private final NumberFormat ni = NumberFormat.getIntegerInstance();
    {
        ni.setGroupingUsed(true);
    }

    public String[] getFormattedRow(R rowId) {
        String[] r = new String[columnKeys.size()];
        ColumnDataSource<R, ?>[] columnData = data.columns(columnKeys);
        for (int i = 0; i < r.length; i++) {
            ColumnDataSource<R, ?> cds = columnData[i];
            if (cds == null) {
                return Arrays.copyOf(r, i);
            }
            String key = columnKeys.get(i);
            Object x = cds.getValue(rowId);
            if (x == null) {
                r[i] = defaultIfBlank.get(key);
                continue;
            }
            if (x instanceof Number) {
                if (((Number) x).doubleValue() == 0) {
                    r[i] = defaultIfZero.get(key);
                    if (r[i] != null)
                        continue;
                }
            }
            String f = formatStrings.get(key);
            if (f != null)
                r[i] = String.format(f, x);
            else if (x instanceof Number) {
                if (x instanceof BigDecimal || x instanceof Double || x instanceof Float)
                    r[i] = nf.format(x);
                else
                    r[i] = ni.format(x);
            } else {
                r[i] = x.toString();
            }
        }
        return r;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(10 * rowIds.size() * columnKeys.size());
        for (R rowId : rowIds) {
            sb.append(Arrays.toString(getFormattedRow(rowId)));
            sb.append("\n");
        }
        return sb.toString();
    }

    public BigDecimal columnTotal(String columnKey) {
        return data.columnTotal(rowIds, columnKey);
    }

    public BigDecimal columnTotal(int columnIndex) {
        return data.columnTotal(rowIds, columnKeys.get(columnIndex));
    }

}
