//   Copyright 2015 Thilo Planz
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

package sandtools.io;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;

import sandtools.aliyun.OSSListCommand;
import sandtools.sandtool.Command;
import sandtools.sandtool.CommandParameters;

public class ListCommand implements Command {

    public static void listFile(File f) {
        if (f.isFile()) {
            list("file", f.getName(), f.lastModified(), f.length());
        }
        else if (f.isDirectory()) {
            list("dir", f.getName(), f.lastModified(), 0);
        }
    }

    public static void list(String type, String name, long modified, long size) {
        System.out.format("%5s %8s %20s %30s%n", type, FileUtils.byteCountToDisplaySize(size),
                DateFormatUtils.ISO_DATETIME_FORMAT.format(modified), name);
    }

    private void list(String targetSpec, CommandParameters params) {
        if (targetSpec == null)
            return;
        if (!StringUtils.contains(targetSpec, ':')) {
            // a file
            listFile(new File(targetSpec));
        } else if (StringUtils.startsWith(targetSpec, "oss")) {
            OSSListCommand.list(targetSpec, params);
        } else {
            throw new IllegalArgumentException("invalid target format " + targetSpec);
        }
    }

    @Override
    public int main(CommandParameters params) throws Exception {
        // ls target...
        String[] arguments = params.getArguments();

        for (String arg : arguments) {
            list(StringUtils.stripToNull(arg), params);
            System.out.println();
        }

        return 0;
    }

}
