//   Copyright 2014 Thilo Planz
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

package sandtools.io;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;

public class BandwidthLimitedInputStream extends BufferedInputStream {

    /**
     * Constant to specify 100K bits per second throughput
     */
    public static final int ONE_HUNDRED_KBIPS = 1024 * 100 / 8000;

    /**
     * Constant to specify 1M bits per second throughput
     */
    public static final int MBIPS = 1024 * 1024 / 8000;

    private final long rate;

    private long availableRate = 0;

    private long nextRefill = -1;

    public BandwidthLimitedInputStream(InputStream in, int bytesPerMilliSecond) {
        super(in);
        if (bytesPerMilliSecond <= 0) {
            throw new IllegalArgumentException("bandwidth must be at least 1 bit per millisecond, you gave me "
                    + bytesPerMilliSecond);
        }
        rate = bytesPerMilliSecond * 100;
    }

    private void blockToRefill() throws IOException {
        long now = System.currentTimeMillis();
        while (now < nextRefill) {
            try {
                Thread.sleep(nextRefill - now);
            } catch (InterruptedException e) {
                throw new IOException(e);
            }
            now = System.currentTimeMillis();
        }

        availableRate = rate;
        nextRefill = now + 100;
    }

    @Override
    public synchronized int read() throws IOException {
        int read = super.read();
        if (read == -1)
            return -1;

        if (availableRate <= 0)
            blockToRefill();

        availableRate--;
        return read;
    }

    @Override
    public synchronized int read(byte[] b, int off, int len) throws IOException {
        // enough to read everything ?
        if (len <= availableRate) {
            int read = super.read(b, off, len);
            if (read == -1)
                return read;
            availableRate -= read;
            return read;
        }
        // can at least read a part ?
        if (availableRate > 0) {
            return read(b, off, (int) availableRate);
        }
        // cannot read anything
        // read anyway, then block before returning
        if (len > rate) {
            // but never more than a single "block"
            len = (int) rate;
        }
        int read = super.read(b, off, len);
        if (read == -1)
            return -1;

        blockToRefill();

        availableRate -= read;
        return read;
    }

}
