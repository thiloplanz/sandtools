//   Copyright 2014, 2015 Thilo Planz
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

package sandtools.io;

import static org.apache.commons.io.filefilter.FileFilterUtils.ageFileFilter;
import static org.apache.commons.io.filefilter.FileFilterUtils.directoryFileFilter;

import java.io.File;
import java.io.FileFilter;
import java.util.Date;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.apache.commons.lang3.time.DateUtils;

import sandtools.aliyun.OSSPurgeCommand;
import sandtools.aws.S3PurgeCommand;
import sandtools.sandtool.Command;
import sandtools.sandtool.CommandParameters;
import sandtools.sandtool.LogUtils;

public class PurgeCommand implements Command {

    private void purgeDirectory(String target, Date cutoff, boolean recurse, boolean removeEmptyDirectories) {
        String[] tp = StringUtils.split(target, ':');
        Validate.inclusiveBetween(2, 2, tp.length, "invalid target " + target);
        File directory = new File(tp[1]);
        AtomicInteger fileCounter = new AtomicInteger(0);
        AtomicLong byteCounter = new AtomicLong(0);
        AtomicInteger directoryCounter = new AtomicInteger(0);

        purgeDirectory(directory, cutoff, recurse, removeEmptyDirectories, fileCounter, byteCounter, directoryCounter);
        if (fileCounter.get() > 0) {
            LogUtils.LOGGER.info("purged {} files ({} bytes) from {}", fileCounter, byteCounter, target);
        }
        if (directoryCounter.get() > 0) {
            LogUtils.LOGGER.info("purged {} empty directories from {}", directoryCounter, target);
        }

    }

    /**
     * @return true, if the directory is now empty
     */
    private boolean purgeDirectory(File directory, Date cutoff, boolean recurse, boolean removeEmptyDirectories,
            AtomicInteger fileCounter, AtomicLong byteCounter, AtomicInteger directoryCounter) {
        if (!directory.isDirectory())
            throw new IllegalArgumentException(directory.getAbsolutePath() + " is not a directory");

        boolean isEmpty = true;

        if (recurse) {
            File[] directoryList = directory.listFiles((FileFilter) directoryFileFilter());
            if (directoryList != null) {
                for (File d : directoryList) {
                    if (purgeDirectory(d, cutoff, recurse, removeEmptyDirectories, fileCounter, byteCounter,
                            directoryCounter) && removeEmptyDirectories) {
                        if (d.delete()) {
                            directoryCounter.incrementAndGet();
                        }
                    } else {
                        isEmpty = false;
                    }
                }
            }
        } else if (removeEmptyDirectories) {
            // we still need to list directories if we want to delete empty ones
            File[] directoryList = directory.listFiles((FileFilter) directoryFileFilter());
            if (directoryList != null) {
                for (File d : directoryList) {
                    if (ArrayUtils.isEmpty(d.list())) {
                        if (d.delete()) {
                            directoryCounter.incrementAndGet();
                        }
                    } else {
                        isEmpty = false;
                    }
                }
            }
        }

        File[] fileList = directory.listFiles((FileFilter) ageFileFilter(cutoff, true));

        int files = 0;
        long bytes = 0;

        if (fileList != null) {
            for (File f : fileList) {
                if (f.isFile()) {
                    long b = f.length();
                    if (FileUtils.deleteQuietly(f)) {
                        files++;
                        bytes += b;
                    } else {
                        LogUtils.LOGGER.warn("failed to delete {}", f.getAbsolutePath());
                        isEmpty = false;
                    }
                } else {
                    isEmpty = false;
                }
            }
        }

        if (files > 0) {
            fileCounter.addAndGet(files);
            byteCounter.addAndGet(bytes);
        }

        if (isEmpty) {
            // check if there are no other files
            isEmpty = ArrayUtils.isEmpty(directory.list());
        }
        return isEmpty;
    }

    @Override
    public int main(CommandParameters params) throws Exception {
        String target = params.getRequiredString("purge.target");
        String[] tp = StringUtils.split(target, ':');
        Date cutoff = DateUtils.addDays(new Date(), -params.getRequiredInteger("purge.days", 1, 10000));
        boolean emptyDirectories = params.getRequiredBoolean("purge.emptyDirectories");

        if ("s3".equals(tp[0])) {
            if (emptyDirectories)
                throw new UnsupportedOperationException("purging empty directories is not supported for S3");
            S3PurgeCommand.purgeFiles(target, cutoff, params);
            return 0;
        }

        if (StringUtils.startsWith(tp[0], "oss")){
            if (emptyDirectories)
                throw new UnsupportedOperationException("purging empty directories is not supported for OSS");
            OSSPurgeCommand.purgeFiles(target, cutoff, params);
            return 0;
        }
        
        if ("directory".equals(tp[0])) {
            purgeDirectory(target, cutoff, false, emptyDirectories);
            return 0;
        }

        if ("tree".equals(tp[0])) {
            purgeDirectory(target, cutoff, true, emptyDirectories);
            return 0;
        }

        throw new IllegalArgumentException("invalid purge target " + target);
    }

}
