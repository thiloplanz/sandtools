//   Copyright 2015 Thilo Planz
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

package sandtools.aliyun;

import java.io.File;

import org.apache.commons.lang3.StringUtils;

import com.aliyun.openservices.oss.OSSClient;
import com.aliyun.openservices.oss.model.ListMultipartUploadsRequest;
import com.aliyun.openservices.oss.model.MultipartUpload;
import com.aliyun.openservices.oss.model.MultipartUploadListing;
import com.aliyun.openservices.oss.model.OSSObjectSummary;
import com.aliyun.openservices.oss.model.ObjectListing;

import sandtools.io.ListCommand;
import sandtools.sandtool.CommandParameters;

public class OSSListCommand {

    static OSSFile parseMultipartUploadTarget(String targetSpec) {
        // oss-upload:BUCKETNAME or oss-upload:HOSTNAME:BUCKETNAME
        String[] parts = StringUtils.split(targetSpec, ':');
        if (parts.length == 2) {
            return new OSSFile(null, parts[1], "dummy");
        } else if (parts.length == 3) {
            return new OSSFile("http://" + parts[1], parts[2], "dummy");
        } else {
            throw new IllegalArgumentException(
                    "invalid OSS target format should be oss-upload:[HOSTNAME:]BUCKETNAME, not " + targetSpec);
        }
    }

    public static void list(String targetSpec, CommandParameters params) {
        if (StringUtils.startsWith(targetSpec, "oss-upload")) {
            OSSFile f = parseMultipartUploadTarget(targetSpec);
            OSSClient client = OSSCopyCommand.getOSSClient(f, params);
            MultipartUploadListing mul = client.listMultipartUploads(new ListMultipartUploadsRequest(f.bucket));
            for (MultipartUpload m : mul.getMultipartUploads()) {
                ListCommand.list("oss-upload", m.getKey() + " " + m.getUploadId(), m.getInitiated().getTime(), 0);
            }
            return;
        }

        Object target = OSSCopyCommand.parseTarget(targetSpec);
        if (target instanceof File) {
            ListCommand.listFile((File) target);
        } else if (target instanceof OSSFile) {
            OSSFile f = (OSSFile) target;
            OSSClient client = OSSCopyCommand.getOSSClient(f, params);
            ObjectListing o = client.listObjects(f.bucket, f.key);
            for (OSSObjectSummary summ : o.getObjectSummaries()) {
                ListCommand.list("oss", summ.getKey(), summ.getLastModified().getTime(), summ.getSize());
            }
            if (o.isTruncated()) {
                System.out.println(" ... has more. Please specify a more precise prefix than " + f.key);
            }
        } else {
            throw new UnsupportedOperationException(targetSpec);
        }
    }

}
