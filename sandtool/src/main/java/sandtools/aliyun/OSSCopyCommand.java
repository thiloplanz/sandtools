//   Copyright 2015 Thilo Planz
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

package sandtools.aliyun;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.input.CountingInputStream;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.StopWatch;

import sandtools.sandtool.Command;
import sandtools.sandtool.CommandParameters;
import sandtools.sandtool.LogUtils;

import com.aliyun.openservices.ClientException;
import com.aliyun.openservices.oss.OSSClient;
import com.aliyun.openservices.oss.model.AbortMultipartUploadRequest;
import com.aliyun.openservices.oss.model.CompleteMultipartUploadRequest;
import com.aliyun.openservices.oss.model.CompleteMultipartUploadResult;
import com.aliyun.openservices.oss.model.InitiateMultipartUploadRequest;
import com.aliyun.openservices.oss.model.InitiateMultipartUploadResult;
import com.aliyun.openservices.oss.model.ObjectMetadata;
import com.aliyun.openservices.oss.model.PartETag;
import com.aliyun.openservices.oss.model.UploadPartRequest;
import com.aliyun.openservices.oss.model.UploadPartResult;

public class OSSCopyCommand implements Command {

    static Object parseTarget(String x) {
        // oss:BUCKETNAME:path or oss:HOSTNAME:BUCKETNAME:path
        // others must not contain a ":" and are treated as local file paths
        String[] parts = StringUtils.split(x, ':');
        if (parts.length == 1)
            return new File(x);
        if (!"oss".equals(parts[0]))
            throw new IllegalArgumentException("unsupported target, only local files and OSS are implemented, not " + x);
        if (parts.length == 3) {
            return new OSSFile(null, parts[1], parts[2]);
        }
        if (parts.length == 4) {
            return new OSSFile("http://" + parts[1], parts[2], parts[3]);
        }

        throw new IllegalArgumentException("invalid OSS target format should be oss:[HOSTNAME:]BUCKETNAME:key, not "
                + x);
    }

    static OSSClient getOSSClient(OSSFile x, CommandParameters params) {
        String accessKey = params.getRequiredString("oss." + x.bucket + ".accessKey");
        String secretKey = params.getRequiredString("oss.secretKey." + accessKey);
        return x.endpoint == null ? new OSSClient(accessKey, secretKey) : new OSSClient(x.endpoint, accessKey,
                secretKey);
    }

    private InputStream getInputStream(Object source, CommandParameters params) throws IOException {
        if (source instanceof File) {
            return new FileInputStream((File) source);
        }
        if (source instanceof OSSFile) {
            OSSFile oss = (OSSFile) source;
            OSSClient client = getOSSClient(oss, params);
            return client.getObject(oss.bucket, oss.key).getObjectContent();
        }
        throw new IllegalArgumentException(source.getClass().getName());
    }

    private long multiPartUpload(OSSClient client, File source, OSSFile target) throws IOException {
        StopWatch watch = LogUtils.startWatch();
        final long fiveMinutes = TimeUnit.MINUTES.toMillis(5);
        long laptime = fiveMinutes;

        long length = source.length();

        final int partSize = 1024 * 1024 * 10;
        int partCount = (int) (length / partSize);
        if (length % partSize != 0) {
            partCount++;
        }

        // read each chunk into memory, so that we can retry more easily
        byte[] buffer = new byte[partSize];

        InitiateMultipartUploadResult initiateMultipartUploadResult = client
                .initiateMultipartUpload(new InitiateMultipartUploadRequest(target.bucket, target.key));
        LogUtils.LOGGER.debug("initiated {}-part multipart upload to {} for {} ({} bytes): {}", partCount, target.key,
                source.getAbsolutePath(), length, initiateMultipartUploadResult.getUploadId());

        try {
            List<PartETag> partETags = new ArrayList<>();
            try (FileInputStream fis = new FileInputStream(source)) {
                for (int i = 1; i <= partCount; i++) {

                    long size = partSize;
                    if (i == partCount) {
                        size = length - ((long) partSize) * (partCount - 1);
                    }
                    IOUtils.readFully(fis, buffer, 0, (int) size);

                    int tries = 1;
                    while (true)
                        try {
                            UploadPartRequest uploadPartRequest = new UploadPartRequest();
                            uploadPartRequest.setBucketName(target.bucket);
                            uploadPartRequest.setKey(target.key);
                            uploadPartRequest.setUploadId(initiateMultipartUploadResult.getUploadId());
                            uploadPartRequest.setInputStream(new ByteArrayInputStream(buffer, 0, (int) size));
                            uploadPartRequest.setPartSize(size);
                            uploadPartRequest.setPartNumber(i);

                            UploadPartResult uploadPartResult = client.uploadPart(uploadPartRequest);
                            partETags.add(uploadPartResult.getPartETag());
                            long now = watch.getTime();
                            if (now > laptime) {
                                LogUtils.lapTime(now, "ongoing upload to \"{}\", {}% done", target.key, 100 * i
                                        / partCount);
                                laptime += fiveMinutes;
                            }
                            break;
                        } catch (ClientException e) {
                            if (tries < 3) {
                                LogUtils.LOGGER.warn("problem uploading part {}, will retry", i, e.toString());
                                tries++;
                            } else {
                                LogUtils.LOGGER.error("problem uploading part {}, giving up", i, e.toString());
                                throw new IOException(e);
                            }
                        }
                }
            }

            CompleteMultipartUploadResult completeMultipartUploadResult = client
                    .completeMultipartUpload(new CompleteMultipartUploadRequest(target.bucket, target.key,
                            initiateMultipartUploadResult.getUploadId(), partETags));

            LogUtils.LOGGER.debug("completed multipart upload {} with ETag {}",
                    initiateMultipartUploadResult.getUploadId(), completeMultipartUploadResult.getETag());
            return length;
        } catch (IOException e) {
            LogUtils.LOGGER.error("cancelling multipart upload {} because of {}",
                    initiateMultipartUploadResult.getUploadId(), e.toString());
            try {
                client.abortMultipartUpload(new AbortMultipartUploadRequest(target.bucket, target.key,
                        initiateMultipartUploadResult.getUploadId()));
            } catch (Exception f) {
                LogUtils.LOGGER.error("failed to cancel multipart upload {}",
                        initiateMultipartUploadResult.getUploadId(), f);
            }
            throw e;
        }
    }

    private long copyTo(Object source, Object target, CommandParameters params) throws IOException {
        CountingInputStream in = new CountingInputStream(getInputStream(source, params));
        try {
            if (target instanceof File) {
                FileUtils.copyInputStreamToFile(in, (File) target);
                return in.getByteCount();
            }
            if (target instanceof OSSFile && source instanceof File) {
                File f = (File) source;
                OSSFile oss = (OSSFile) target;
                OSSClient client = getOSSClient(oss, params);

                if (f.length() > 100 * 1024 * 1024)
                    return multiPartUpload(client, f, oss);
                ObjectMetadata meta = new ObjectMetadata();
                meta.setContentLength(f.length());
                client.putObject(oss.bucket, oss.key, in, meta);
                return in.getByteCount();
            }
        } finally {
            IOUtils.closeQuietly(in);
        }
        throw new IllegalArgumentException();
    }

    @Override
    public int main(CommandParameters params) throws Exception {
        // osscp FROM TO

        String[] arguments = params.getArguments();

        if (arguments.length != 2)
            throw new IllegalArgumentException("must specify FROM and TO, not " + Arrays.toString(arguments));

        Object from = parseTarget(arguments[0]);
        if (from instanceof File) {
            File f = (File) from;
            // only supports single files (not directories)
            if (!f.isFile())
                throw new UnsupportedOperationException("can only copy files, not " + f.getAbsolutePath());
            if (!f.canRead())
                throw new IllegalArgumentException("cannot read " + f.getAbsolutePath());
        }

        Object to = parseTarget(arguments[1]);
        if (to instanceof File) {
            File f = (File) to;
            if (f.isDirectory()) {
                // use the same file name
                if (from instanceof File) {
                    to = new File(f, ((File) from).getName());
                } else {
                    to = new File(f, FilenameUtils.getName(((OSSFile) from).key));
                }
            }
        }

        // only supports from OSS to local or vice versa
        if (from instanceof OSSFile && to instanceof OSSFile)
            throw new UnsupportedOperationException("copying between two OSS buckets is not yet implemented");

        copyTo(from, to, params);

        return 0;
    }
}
