//   Copyright 2015 Thilo Planz
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

package sandtools.aliyun;

import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.StopWatch;

import sandtools.sandtool.CommandParameters;
import sandtools.sandtool.LogUtils;

import com.aliyun.openservices.oss.OSSClient;
import com.aliyun.openservices.oss.model.AbortMultipartUploadRequest;
import com.aliyun.openservices.oss.model.ListMultipartUploadsRequest;
import com.aliyun.openservices.oss.model.MultipartUpload;
import com.aliyun.openservices.oss.model.MultipartUploadListing;

public class OSSPurgeCommand {
    public static void purgeFiles(String target, Date cutoff, CommandParameters params) throws Exception {
        String[] tp = StringUtils.split(target, ':');
        if (!"oss-upload".equals(tp[0]))
            throw new IllegalArgumentException("invalid purge target, only oss-upload is supported " + target);

        OSSFile f = OSSListCommand.parseMultipartUploadTarget(target);
        OSSClient client = OSSCopyCommand.getOSSClient(f, params);

        StopWatch s = LogUtils.startWatch();
        int count = 0;
        MultipartUploadListing mul = client.listMultipartUploads(new ListMultipartUploadsRequest(f.bucket));
        for (MultipartUpload m : mul.getMultipartUploads()) {
            if (cutoff.after(m.getInitiated())) {
                client.abortMultipartUpload(new AbortMultipartUploadRequest(mul.getBucketName(), m.getKey(), m
                        .getUploadId()));
                count++;
            }
        }
        if (count > 0)
            LogUtils.stopWatch(s, "abort {} multi-part uploads from {}", count, f.bucket);
    }
}
