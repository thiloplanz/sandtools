//   Copyright 2015 Thilo Planz
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

package sandtools.mail;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.nio.file.Files;

import org.apache.commons.io.Charsets;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.entity.mime.content.ContentBody;

/**
 * Debug tool. Instead of sending email, dumps it into a directory.
 * 
 * Use with mail API spec "dump:/path/to/directory"
 * 
 */

class MailDumper extends MailSender {

    private final File dumpDirectory;

    MailDumper(String apiSpec) {
        // dump:directory
        String[] parts = StringUtils.split(apiSpec, ':');
        if (parts.length != 2)
            throw new IllegalArgumentException(
                    "invalid email dumper specification, should be 'dump:/path/to/directory' , not " + apiSpec);
        dumpDirectory = new File(parts[1]);
        if (!dumpDirectory.isDirectory()) {
            dumpDirectory.mkdirs();
        }
        if (!dumpDirectory.isDirectory()) {
            throw new IllegalArgumentException("email dump directory '" + dumpDirectory.getAbsolutePath()
                    + "' does not exist and could not be created.");
        }
    }

    @Override
    void doSendMail(String to, String replyTo, String subject, String body, ContentBody... attachments)
            throws IOException {
        File mailBody = File.createTempFile("email_", ".txt", dumpDirectory);
        try (PrintWriter w = new PrintWriter(Files.newBufferedWriter(mailBody.toPath(), Charsets.UTF_8))) {
            w.format("To: %s%n", to);
            if (StringUtils.isNotBlank(replyTo))
                w.format("Reply-To: %s%n", replyTo);
            if (StringUtils.isNotBlank(subject))
                w.format("Subject: %s%n", subject);
            w.println();
            w.println(body);
        }
        int i = 1;
        String format = mailBody.getName().replace(".txt", "-%d-%s");
        for (ContentBody b : attachments) {
            File attachment = new File(dumpDirectory, String.format(format, i++, b.getFilename()));
            try (OutputStream o = new BufferedOutputStream(new FileOutputStream(attachment))) {
                b.writeTo(o);
            }
        }
    }
}
