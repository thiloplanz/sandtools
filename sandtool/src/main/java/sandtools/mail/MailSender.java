//   Copyright 2015 Thilo Planz
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

package sandtools.mail;

import static sandtools.sandtool.LogUtils.LOGGER;

import java.io.IOException;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.StopWatch;
import org.apache.http.entity.mime.content.ContentBody;

import sandtools.sandtool.LogUtils;

public abstract class MailSender {

    public final void sendMail(String to, String subject, String body, ContentBody... attachments) throws IOException {
        sendMail(to, null, subject, body, attachments);
    }

    public final void sendMail(String to, String replyTo, String subject, String body, ContentBody... attachments)
            throws IOException {
        StopWatch stopWatch = LogUtils.startWatch();

        doSendMail(to, replyTo, subject, body, attachments);
        if (LOGGER.isDebugEnabled()) {
            LogUtils.stopWatchDebug(stopWatch, "sent email '{}' to '{}'", subject, to);
            if (attachments.length > 0) {
                for (ContentBody b : attachments) {
                    LOGGER.debug("  with {} attachment '{}' ({} bytes)", b.getMimeType(), b.getFilename(),
                            b.getContentLength());
                }
            }
        }

    }

    abstract void doSendMail(String to, String replyTo, String subject, String body, ContentBody... attachments)
            throws IOException;

    public static MailSender getMailSender(String apiSpec) {
        apiSpec = StringUtils.stripToNull(apiSpec);
        if (StringUtils.startsWith(apiSpec, "sendcloud:"))
            return new SendCloud(apiSpec);
        if (StringUtils.startsWith(apiSpec, "dump:"))
            return new MailDumper(apiSpec);
        throw new IllegalArgumentException("invalid email sending api specification " + apiSpec);
    }

}
