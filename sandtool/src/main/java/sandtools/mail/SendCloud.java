//   Copyright 2014, 2015 Thilo Planz
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

package sandtools.mail;

import java.io.IOException;

import org.apache.commons.io.Charsets;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.ContentBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

class SendCloud extends MailSender {

    private final static ContentType TEXT_UTF_8 = ContentType.TEXT_PLAIN.withCharset(Charsets.UTF_8);

    private final String username;

    private final String key;

    private final String from;

    private final String fromName;

    SendCloud(String apiSpec) {
        // sendcloud:username:key:from[:fromName]
        String[] parts = StringUtils.split(apiSpec, ':');
        if (parts.length != 5 && parts.length != 4)
            throw new IllegalArgumentException("invalid sendcloud api specification " + apiSpec);
        if (!"sendcloud".equals(parts[0]))
            throw new IllegalArgumentException("invalid sendcloud api specification " + apiSpec);
        username = parts[1];
        key = parts[2];
        from = parts[3];
        fromName = parts.length > 4 ? parts[4] : null;
    }

    public void doSendMail(String to, String replyTo, String subject, String body, ContentBody... attachments)
            throws IOException {
        final String url = "https://sendcloud.sohu.com/webapi/mail.send.xml";

        HttpPost httpPost = new HttpPost(url);
        try {
            HttpClient httpclient = HttpClients.createDefault();

            MultipartEntityBuilder entityBuilder = MultipartEntityBuilder.create();
            entityBuilder.addPart("api_user", new StringBody(username, TEXT_UTF_8));
            entityBuilder.addPart("api_key", new StringBody(key, TEXT_UTF_8));
            entityBuilder.addPart("to", new StringBody(to, TEXT_UTF_8));
            entityBuilder.addPart("from", new StringBody(from, TEXT_UTF_8));
            if (StringUtils.isNotBlank(replyTo))
                entityBuilder.addPart("replyto", new StringBody(replyTo, TEXT_UTF_8));
            if (StringUtils.isNotBlank(fromName))
                entityBuilder.addPart("fromname", new StringBody(fromName, TEXT_UTF_8));
            if (StringUtils.isNotBlank(subject))
                entityBuilder.addPart("subject", new StringBody(subject, TEXT_UTF_8));
            if (StringUtils.isNotBlank(body))
                entityBuilder.addPart("html", new StringBody(body, TEXT_UTF_8));

            for (ContentBody a : attachments)
                entityBuilder.addPart("file", a);

            httpPost.setEntity(entityBuilder.build());

            HttpResponse response = httpclient.execute(httpPost);
            String result = IOUtils.toString(response.getEntity().getContent(), Charsets.UTF_8);
            EntityUtils.consume(httpPost.getEntity());

            if (response.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
                throw new IOException(response.getStatusLine().getReasonPhrase());
            }
            if (!result.contains("<result><message>success</message></result>"))
                throw new IOException(result);

        } finally {
            httpPost.releaseConnection();
        }
    }

}
