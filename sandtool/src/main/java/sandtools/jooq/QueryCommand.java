//   Copyright 2014, 2015 Thilo Planz
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

package sandtools.jooq;

import static sandtools.sandtool.LogUtils.LOGGER;
import static sandtools.sandtool.LogUtils.startWatch;
import static sandtools.sandtool.LogUtils.stopWatchDebug;
import static sandtools.sandtool.LogUtils.stopWatchTrace;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.lang3.time.StopWatch;
import org.h2.Driver;
import org.jooq.Cursor;
import org.jooq.Record;
import org.jooq.Result;
import org.jooq.impl.DSL;
import org.jooq.tools.jdbc.JDBCUtils;

import sandtools.sandtool.Command;
import sandtools.sandtool.CommandParameters;
import sandtools.sandtool.LogUtils;

public class QueryCommand implements Command {

    static Result<Record> fetch(Connection conn, String query) {
        StopWatch s = startWatch();
        Result<Record> result = DSL.using(conn).resultQuery(query).fetch();
        stopWatchDebug(s, "execute query and fetch {} rows into memory", result.size());
        return result;
    }

    static Result<Record> fetch(Connection conn, ResultSet rs) {
        StopWatch s = startWatch();
        Result<Record> result = DSL.using(conn).fetch(rs);
        stopWatchDebug(s, "fetch {} rows into memory", result.size());
        return result;
    }

    static Cursor<Record> openCursor(Connection conn, String query) {
        StopWatch s = startWatch();
        // https://github.com/jOOQ/jOOQ/issues/3514
        Cursor<Record> result = DSL.using(conn).resultQuery(query).fetchLazy(100);
        stopWatchTrace(s, "execute query and open query result cursor");
        if (!result.hasNext())
            LOGGER.debug("result cursor is empty");

        return result;
    }

    static Cursor<Record> openCursor(Connection conn, ResultSet rs) {
        StopWatch s = startWatch();
        Cursor<Record> result = DSL.using(conn).fetchLazy(rs);
        stopWatchTrace(s, "open query result cursor");
        if (!result.hasNext())
            LOGGER.debug("result cursor is empty");

        return result;
    }

    public int main(CommandParameters params) throws IOException, SQLException {

        String query = params.getRequiredString("query");

        List<QueryAction> actions = params.getRequiredActionList(QueryAction.values());

        Connection conn = connectToDatabase(params);
        try {
            if (actions.size() > 1) {
                Result<Record> result = fetch(conn, query);
                for (QueryAction action : actions) {
                    action.processResult(params, result);
                }
            } else {
                // for Postgresql, connection cannot be in auto-commit mode for
                // result set streaming to work
                // http://jdbc.postgresql.org/documentation/head/query.html#query-with-cursor
                boolean autoCommit = conn.getAutoCommit();
                if (autoCommit)
                    conn.setAutoCommit(false);
                try {
                    Cursor<Record> result = openCursor(conn, query);
                    try {
                        if (!actions.isEmpty())
                            actions.get(0).processCursor(params, result);
                    } finally {
                        result.close();
                    }
                } finally {
                    if (autoCommit)
                        conn.setAutoCommit(true);
                }
            }
        } finally {
            JDBCUtils.safeClose(conn);
        }
        return 0;
    }

    private Connection connectToDatabase(CommandParameters params) throws SQLException {
        String url = params.getRequiredString("query.url");
        String user = params.getRequiredString("query.username");
        String password = params.getRequiredString("query.password");

        // make sure the driver is registered
        if (url.startsWith("jdbc:h2:")) {
            try {
                new Driver();
            } catch (Exception e) {
                LogUtils.LOGGER.debug("failed to load default H2 database driver", e);
            }
        }

        return DriverManager.getConnection(url, user, password);
    }

}
