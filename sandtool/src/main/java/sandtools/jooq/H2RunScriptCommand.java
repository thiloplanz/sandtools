//   Copyright 2014, 2015 Thilo Planz
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

package sandtools.jooq;

import static sandtools.jooq.QueryCommand.fetch;
import static sandtools.jooq.QueryCommand.openCursor;
import static sandtools.sandtool.LogUtils.startWatch;
import static sandtools.sandtool.LogUtils.stopWatch;

import java.io.IOException;
import java.io.StringReader;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.apache.commons.lang3.time.StopWatch;
import org.h2.util.ScriptReader;
import org.jooq.Cursor;
import org.jooq.Record;
import org.jooq.Result;
import org.jooq.tools.jdbc.JDBCUtils;

import sandtools.sandtool.Command;
import sandtools.sandtool.CommandParameters;

public class H2RunScriptCommand implements Command {

    private List<String> readStatements(String s) {
        ScriptReader r = new ScriptReader(new StringReader(s));
        List<String> l = new ArrayList<>();
        while (true) {
            String sql = StringUtils.stripToNull(r.readStatement());
            if (sql == null) {
                break;
            }
            l.add(sql);
        }
        r.close();
        return l;
    }

    public int main(CommandParameters params) throws SQLException, IOException {

        String script = params.getRequiredString("script");

        List<QueryAction> actions = params.getRequiredActionList(QueryAction.values());

        List<String> sql = readStatements(script);

        Connection conn = H2Utils.connect(params, "h2script.file", true);
        try {
            Statement st = conn.createStatement();
            ResultSet rs = null;
            for (String s : sql) {
                JDBCUtils.safeClose(rs);
                rs = null;

                StopWatch w = startWatch();
                int updateCount = -1;
                if (st.execute(s)) {
                    rs = st.getResultSet();
                } else {
                    updateCount = st.getUpdateCount();
                }
                s = StringUtils.abbreviate(StringUtils.normalizeSpace(s), 75);
                if (updateCount > -1) {
                    s = String.format("%s (%d rows)", s, updateCount);
                }
                stopWatch(w, "run query {} ", s);
            }

            if (actions.isEmpty())
                JDBCUtils.safeClose(rs);
            else if (actions.size() > 1) {
                Validate.notNull(rs, "no result set returned from query");
                Result<Record> result = fetch(conn, rs);
                for (QueryAction action : actions) {
                    action.processResult(params, result);
                }
            } else {
                Validate.notNull(rs, "no result set returned from query");
                Cursor<Record> result = openCursor(conn, rs);
                try {
                    actions.get(0).processCursor(params, result);
                } finally {
                    result.close();
                }

            }

        } finally {
            JDBCUtils.safeClose(conn);
        }
        return 0;
    }
}
