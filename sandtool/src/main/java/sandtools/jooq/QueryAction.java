//   Copyright 2014, 2015 Thilo Planz
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

package sandtools.jooq;

import static org.apache.commons.lang3.ObjectUtils.defaultIfNull;
import static sandtools.sandtool.CommandUtils.writeToFileOrSystemOut;
import static sandtools.sandtool.LogUtils.startWatch;
import static sandtools.sandtool.LogUtils.stopWatchTrace;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.SQLException;

import org.apache.commons.io.IOUtils;
import org.apache.commons.io.output.ByteArrayOutputStream;
import org.apache.commons.lang3.time.StopWatch;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.content.ByteArrayBody;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.jooq.Cursor;
import org.jooq.DSLContext;
import org.jooq.Field;
import org.jooq.Record;
import org.jooq.Result;
import org.jooq.impl.DSL;
import org.jooq.tools.jdbc.JDBCUtils;

import sandtools.mail.MailSender;
import sandtools.sandtool.CommandParameters;

enum QueryAction {

    makeCSV, makeExcel, makeJSON, makeXML, mailExcel, makeH2, summarize;

    void processResult(CommandParameters controlFile, Result<Record> result) throws IOException, SQLException {

        switch (this) {
        case makeCSV: {
            File outputFile = controlFile.getOptionalOutputFile(name() + ".file");

            writeToFileOrSystemOut(outputFile, result.formatCSV());
            return;
        }
        case makeExcel: {
            File outputFile = controlFile.getOptionalOutputFile(name() + ".file");

            Workbook excel = makeExcel(result.fields(), result);
            OutputStream out = System.out;
            if (outputFile != null) {
                out = new FileOutputStream(outputFile);
            }
            try {
                excel.write(out);
            } finally {
                if (outputFile != null)
                    IOUtils.closeQuietly(out);
            }
            return;
        }
        case makeJSON: {
            File outputFile = controlFile.getOptionalOutputFile(name() + ".file");

            writeToFileOrSystemOut(outputFile, result.formatJSON());
            writeToFileOrSystemOut(outputFile, "\n");
            return;
        }
        case makeXML: {
            File outputFile = controlFile.getOptionalOutputFile(name() + ".file");

            writeToFileOrSystemOut(outputFile, result.formatXML());
            writeToFileOrSystemOut(outputFile, "\n");
            return;
        }
        case mailExcel:
            if (result.isNotEmpty())
                mailExcel(controlFile, result.fields(), result);
            return;
        case makeH2:
            makeH2(controlFile, result.fields(), result);
            return;
        case summarize:
            summarize(result.fields(), result);
            return;
        default:
            throw new UnsupportedOperationException(this.name());
        }

    }

    private Result<Record> fetch(Cursor<Record> cursor) {
        StopWatch s = startWatch();
        Result<Record> result = cursor.fetch();
        stopWatchTrace(s, "fetch {} rows into memory", result.size());
        return result;
    }

    void processCursor(CommandParameters controlFile, Cursor<Record> cursor) throws IOException, SQLException {

        switch (this) {
        case makeCSV:
        case makeJSON:
        case makeXML: {
            processResult(controlFile, fetch(cursor));
            return;
        }
        case makeExcel: {
            File outputFile = controlFile.getOptionalOutputFile(name() + ".file");

            Workbook excel = makeExcel(cursor.fields(), cursor);
            OutputStream out = System.out;
            if (outputFile != null) {
                out = new FileOutputStream(outputFile);
            }
            try {
                excel.write(out);
            } finally {
                if (outputFile != null)
                    IOUtils.closeQuietly(out);
            }
            return;
        }
        case mailExcel:
            if (cursor.hasNext())
                mailExcel(controlFile, cursor.fields(), cursor);
            return;
        case makeH2:
            makeH2(controlFile, cursor.fields(), cursor);
            return;
        case summarize:
            summarize(cursor.fields(), cursor);
            return;
        default:
            throw new UnsupportedOperationException(this.name());
        }

    }

    private void mailExcel(CommandParameters controlFile, Field<?>[] fields, Iterable<Record> result)
            throws IOException {
        MailSender mailer = MailSender.getMailSender(controlFile.getRequiredString("mailExcel.api"));
        String to = controlFile.getRequiredString("mailExcel.to");
        String subject = controlFile.getRequiredString("mailExcel.subject");
        String body = controlFile.getRequiredString("mailExcel.body");
        Workbook excel = makeExcel(fields, result);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        excel.write(baos);
        mailer.sendMail(to, subject, body,
                new ByteArrayBody(baos.toByteArray(), ContentType.create("application/vnd.ms-excel"), "report.xls"));
    }

    private Workbook makeExcel(Field<?>[] fields, Iterable<Record> result) {
        Workbook wb = new HSSFWorkbook();
        Sheet sheet = wb.createSheet("query result");
        CellStyle headerStyle = wb.createCellStyle();
        Font boldFont = wb.createFont();
        boldFont.setBoldweight(Font.BOLDWEIGHT_BOLD);
        headerStyle.setFont(boldFont);
        int rowNum = 0;
        {
            Row header = sheet.createRow(rowNum++);
            int i = 0;
            for (Field<?> f : fields) {
                Cell c = header.createCell(i++);
                c.setCellValue(f.getName());
                c.setCellStyle(headerStyle);
            }
        }
        for (Record r : result) {
            Row row = sheet.createRow(rowNum++);
            for (int i = 0; i < r.size(); i++) {
                Object o = r.getValue(i);
                if (o == null)
                    continue;
                Cell c = row.createCell(i);
                if (o instanceof Number)
                    c.setCellValue(((Number) o).doubleValue());
                else
                    c.setCellValue(o.toString());
            }
        }

        sheet.createFreezePane(0, 1);
        for (int i = 0; i < fields.length; i++) {
            sheet.autoSizeColumn(i);
        }
        return wb;
    }

    private void makeH2(CommandParameters controlFile, Field<?>[] fields, Iterable<Record> result) throws SQLException {

        InsertMode insertMode = controlFile.getRequiredOption(InsertMode.class, "makeH2.mode");
        String tableName = controlFile.getRequiredString("makeH2.table");
        boolean defrag = controlFile.getRequiredBoolean("makeH2.defrag");

        Connection conn = H2Utils.connect(controlFile, "makeH2.file", true);
        try {
            DSLContext dsl = DSL.using(conn);
            if (insertMode == InsertMode.drop) {
                H2Utils.dropTableIfExists(dsl, tableName);
            }
            // turn off auto-commit for faster inserts
            conn.setAutoCommit(false);
            H2Utils.createTableFromResult(dsl, tableName, fields, result, insertMode);
            conn.commit();

            if (defrag) {
                H2Utils.defrag(conn);
            }

        } finally {
            JDBCUtils.safeClose(conn);
        }
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    private void summarize(Field<?>[] fields, Iterable<Record> result) throws SQLException {

        int count = 0;
        int[] notNull = new int[fields.length];
        Comparable<?>[] max = new Comparable<?>[fields.length];
        Comparable<?>[] min = new Comparable<?>[fields.length];

        for (Record r : result) {
            for (int i = 0; i < fields.length; i++) {
                Object x = r.getValue(i);
                if (x != null) {
                    notNull[i]++;
                    if (x instanceof Comparable<?>) {
                        if (max[i] == null)
                            max[i] = (Comparable<?>) x;
                        else {
                            Comparable c = (Comparable) x;
                            if (c.compareTo(max[i]) > 0)
                                max[i] = c;
                        }

                        if (min[i] == null)
                            min[i] = (Comparable<?>) x;
                        else {
                            Comparable c = (Comparable) x;
                            if (c.compareTo(min[i]) < 0)
                                min[i] = c;
                        }
                    }
                }
            }
            count++;
        }

        System.out.format("Number of rows:     %8d%n", count);
        System.out.format("Number of columns:  %8d%n", fields.length);
        if (count > 0) {
            System.out.format("%32s %8s %25s %25s%n", "Column", "Not Null", "Min", "Max");
            for (int i = 0; i < fields.length; i++) {
                System.out.format("%32s %8d %25.25s %25.25s%n", fields[i].getName(), notNull[i],
                        defaultIfNull(min[i], ""), defaultIfNull(max[i], ""));
            }
        }

    }

}
