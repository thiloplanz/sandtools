//   Copyright 2014, 2015 Thilo Planz
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

package sandtools.jooq;

import static sandtools.sandtool.LogUtils.startWatch;
import static sandtools.sandtool.LogUtils.stopWatch;

import java.io.File;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.StopWatch;
import org.h2.Driver;
import org.jooq.BatchBindStep;
import org.jooq.Configuration;
import org.jooq.DSLContext;
import org.jooq.DataType;
import org.jooq.Field;
import org.jooq.Record;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;
import org.jooq.impl.DefaultConfiguration;

import sandtools.sandtool.CommandParameters;
import sandtools.sandtool.LogUtils;

class H2Utils {

    static Connection connect(CommandParameters controlFile, String key, boolean create) throws SQLException {
        return connect(new File(controlFile.getRequiredString(key)), create);
    }

    static Connection connect(File dbFile, boolean create) throws SQLException {
        String ifexists = create ? "" : ";IFEXISTS=TRUE";
        String url = "jdbc:h2:" + dbFile.getAbsolutePath() + ifexists;
        return new Driver().connect(url, null);
    }

    static void dropTableIfExists(DSLContext dsl, String name) {
        dsl.execute("DROP TABLE IF EXISTS " + name);
    }

    static void createTableFromResult(DSLContext dsl, String tableName, Field<?>[] fields, Iterable<Record> result,
            InsertMode insertMode) {
        createTableFromResult(SQLDialect.H2, dsl, tableName, fields, result, insertMode);
    }

    private static void createTableFromResult(SQLDialect dialect, DSLContext dsl, String tableName, Field<?>[] fields,
            Iterable<Record> result, InsertMode insertMode) {
        Configuration derbyConf = new DefaultConfiguration().derive(dialect);

        StringBuilder sql = new StringBuilder(500);

        StringBuilder insert = new StringBuilder(200);

        sql.append("create table ");
        sql.append(tableName);
        sql.append(" (");
        insert.append("insert into ");
        insert.append(tableName);
        insert.append(" values (");
        for (Field<?> f : fields) {
            sql.append(f.getName());
            sql.append(" ");
            DataType<?> t = f.getDataType(derbyConf);
            sql.append(t.getSQLDataType().getTypeName());
            if (t.hasLength()) {
                sql.append("(");
                sql.append(t.length());
                sql.append(")");
            } else {
                if (t.hasPrecision()) {
                    sql.append("(");
                    sql.append(t.precision());
                    if (t.hasScale()) {
                        sql.append(",");
                        sql.append(t.scale());
                    }
                    sql.append(")");
                }
            }
            insert.append("?,");
            sql.append(",\n");
        }
        insert.deleteCharAt(insert.length() - 1);
        sql.delete(sql.length() - 2, sql.length());
        sql.append(")");
        insert.append(")");

        if (insertMode == InsertMode.create || insertMode == InsertMode.drop)
            dsl.execute(sql.toString());

        if (insertMode == InsertMode.truncate)
            dsl.truncate(DSL.table(tableName)).execute();

        StopWatch w = startWatch();

        if (insertMode == InsertMode.merge) {
            insert.replace(0, 6, "merge ");
        }

        org.jooq.Query q = dsl.query(insert.toString(), new Object[fields.length]);

        BatchBindStep batch = null;

        final long oneMinute = TimeUnit.MINUTES.toMillis(1);
        long laptime = oneMinute;

        final String insertOrMerge = StringUtils.strip(insert.substring(0, 6));

        int j = 0;
        for (Record r : result) {
            if (j % 500 == 0) {
                if (batch != null)
                    batch.execute();
                long now = w.getTime();
                if (now > laptime) {
                    LogUtils.lapTime(now, "ongoing {} into table \"{}\", {} rows done", insertOrMerge, tableName, j);
                    laptime += oneMinute;
                }
                batch = dsl.batch(q);
            }
            Object[] values = new Object[fields.length];

            for (int i = 0; i < values.length; i++) {
                values[i] = r.getValue(i);
            }
            batch.bind(values);
            j++;
        }
        if (batch != null) {
            batch.execute();
        }

        stopWatch(w, "{} {} rows into table \"{}\"", insertOrMerge, j, tableName);
    }

    /**
     * This also closes the connection and shuts down the database
     * 
     * @param conn
     * @throws SQLException
     */
    static void defrag(Connection conn) throws SQLException {
        StopWatch w = startWatch();
        conn.prepareStatement("SHUTDOWN DEFRAG").execute();
        stopWatch(w, "defragmentation of the database");
    }

}
