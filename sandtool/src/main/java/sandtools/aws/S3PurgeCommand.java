//   Copyright 2014 Thilo Planz
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

package sandtools.aws;

import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.StopWatch;

import sandtools.sandtool.CommandParameters;
import sandtools.sandtool.LogUtils;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.S3ObjectSummary;

public class S3PurgeCommand {

    public static void purgeFiles(String target, Date cutoff, CommandParameters params) throws Exception {
        String[] tp = StringUtils.split(target, ':');
        if (!"s3".equals(tp[0]))
            throw new IllegalArgumentException("invalid purge target, only S3 is supported " + target);
        String bucketName = tp[1];
        String accessKey = params.getRequiredString("purge.s3.accessKey");
        String secretKey = params.getRequiredString("s3.secretKey." + accessKey);
        String prefix = tp[2];

        AWSCredentials credentials = new BasicAWSCredentials(accessKey, secretKey);
        AmazonS3 s3 = new AmazonS3Client(credentials);
        ObjectListing ol = null;

        int files = 0;
        long bytes = 0;

        StopWatch s = LogUtils.startWatch();

        while (ol == null || ol.isTruncated()) {
            ol = ol == null ? s3.listObjects(bucketName, prefix) : s3.listNextBatchOfObjects(ol);
            for (S3ObjectSummary o : ol.getObjectSummaries()) {
                if (cutoff.after(o.getLastModified())) {
                    s3.deleteObject(o.getBucketName(), o.getKey());
                    files++;
                    bytes += o.getSize();
                }
            }
        }
        if (files > 0)
            LogUtils.stopWatch(s, "purge {} files ({} bytes) from {}", files, bytes, target);
    }
}
