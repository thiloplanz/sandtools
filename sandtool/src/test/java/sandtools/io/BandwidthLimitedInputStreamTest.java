//   Copyright 2014 Thilo Planz
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

package sandtools.io;

import static org.junit.Assert.assertTrue;
import static sandtools.io.BandwidthLimitedInputStream.ONE_HUNDRED_KBIPS;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.IOUtils;
import org.apache.commons.io.output.NullOutputStream;
import org.junit.Test;

public class BandwidthLimitedInputStreamTest {

    @Test
    public void testSpeedLimit() throws IOException {
        byte[] data = new byte[10 * 1024];
        InputStream in = new BandwidthLimitedInputStream(new ByteArrayInputStream(data), 5 * ONE_HUNDRED_KBIPS);
        long started = System.nanoTime();
        IOUtils.copy(in, new NullOutputStream());
        long taken = System.nanoTime() - started;
        assertTrue("took some time: " + taken, taken > 100000000);
    }
}
