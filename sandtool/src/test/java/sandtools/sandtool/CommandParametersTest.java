//   Copyright 2014 Thilo Planz
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

package sandtools.sandtool;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.Properties;

import org.apache.commons.io.Charsets;
import org.apache.commons.io.IOUtils;
import org.junit.Assert;
import org.junit.Test;

public class CommandParametersTest {

    @Test
    public void testParsing() throws IOException {
        {
            CommandParameters c = new CommandParameters(new String[] { "dummy", "a", "b", "--x", "123", "--sandtool.command.dummy.class", "dummy" });
            assertArrayEquals(new String[] { "a", "b" }, c.getArguments());
            assertEquals("123", c.getParameters().getProperty("x"));
        }
        {
            CommandParameters c = new CommandParameters(new String[] { "dummy", "--x", "123", "a", "b", "--sandtool.command.dummy.class", "dummy" });
            assertArrayEquals(new String[] { "a", "b" }, c.getArguments());
            assertEquals("123", c.getParameters().getProperty("x"));
        }
    }

    @Test
    public void testConfigurationFileParsing() throws IOException {
        Properties p = CommandParameters.parseConfigurationFile(
                IOUtils.toString(getClass().getResourceAsStream("1.sand"), Charsets.UTF_8), null);
        Assert.assertEquals("This is some text\n", p.getProperty("text"));
    }
    
    @Test
    public void testConfigurationFileParsing_OnlyTextSection() throws IOException {
        Properties p = CommandParameters.parseConfigurationFile(
                IOUtils.toString(getClass().getResourceAsStream("only_text_section.sand"), Charsets.UTF_8), null);
        Assert.assertEquals("This file only has a text section (no properties at all, not even an empty line, before it)", p.getProperty("text"));
    }
}
