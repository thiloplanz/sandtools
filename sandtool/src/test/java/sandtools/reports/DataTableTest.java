//   Copyright 2015 Thilo Planz
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

package sandtools.reports;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

public class DataTableTest {

    private static <T> Map<String, T> map(String k, T v) {
        Map<String, T> row = new HashMap<>();
        row.put(k, v);
        return row;
    }

    @Test
    public void testSingleRow() {
        Map<String, Map<String, String>> data = map("eins", map("col1", "one"));
        data.put("zwei", map("col1", "two"));

        TabularData<String> td = new TabularData<>();
        td.setKeyColumn("key");
        td.setColumnDataSource("english", td.mapEntryColumn(data, "col1"));

        DataTable<String> d = new DataTable<>(td, Arrays.asList("eins")).withColumns("english", "key");

        assertEquals("[one, eins]\n", d.toString());
    }

    @Test
    public void testDefaultNumberFormat() {
        Map<String, Map<String, Double>> data = map("eins", map("col1", 999999.5));

        TabularData<String> td = new TabularData<>();
        td.setKeyColumn("key");
        td.setColumnDataSource("english", td.mapEntryColumn(data, "col1"));

        DataTable<String> d = new DataTable<>(td, Arrays.asList("eins")).withColumns("english", "key");

        assertEquals("[999,999.50, eins]\n", d.toString());
    }

    @Test
    public void testCustomFormat() {
        Map<String, Map<String, Double>> data = map("eins", map("col1", 99.5));

        TabularData<String> td = new TabularData<>();
        td.setColumnDataSource("english", td.mapEntryColumn(data, "col1"));

        DataTable<String> d = new DataTable<>(td, Arrays.asList("eins")).withColumn("english", "%3.1f%%");

        assertEquals("[99.5%]\n", d.toString());
    }

    @Test
    public void testZeroAsBlank() {
        Map<String, Map<String, Integer>> data = map("eins", map("col1", 0));

        TabularData<String> td = new TabularData<>();
        td.setColumnDataSource("english", td.mapEntryColumn(data, "col1"));

        DataTable<String> d = new DataTable<>(td, Arrays.asList("eins")).withColumns("english");
        d.setColumnFormat("english", "%3.1f%%", "-", "");

        assertEquals("[]\n", d.toString());
    }

}
