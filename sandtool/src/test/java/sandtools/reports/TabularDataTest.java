//   Copyright 2015 Thilo Planz
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

package sandtools.reports;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

public class TabularDataTest {

    private static <T> Map<String, T> map(String k, T v) {
        Map<String, T> row = new HashMap<>();
        row.put(k, v);
        return row;
    }

    @Test
    public void testColumnFromMap() {
        Map<String, Map<String, String>> data = map("eins", map("col1", "one"));
        data.put("zwei", map("col1", "two"));

        TabularData<String> td = new TabularData<>();
        td.setKeyColumn("key");
        td.setColumnDataSource("english", td.mapEntryColumn(data, "col1"));

        assertEquals("eins", td.getValue("eins", "key"));
        assertEquals("one", td.getValue("eins", "english"));
        assertEquals("zwei", td.getValue("zwei", "key"));
        assertEquals("two", td.getValue("zwei", "english"));
    }

    @Test
    public void testKeyedNestedMaps() {
        Map<String, Map<String, Map<String, String>>> data = map("eins", map("col", map("1", "one")));

        TabularData<String> td = new TabularData<>();
        td.setKeyColumn("key");
        td.setColumnDataSource("english", td.nestedColumn(td.mapEntryColumn(data, "col"), "1"));

        assertEquals("eins", td.getValue("eins", "key"));
        assertEquals("one", td.getValue("eins", "english"));
        assertEquals("non-existing row", "zwei", td.getValue("zwei", "key"));
        assertEquals("non-existing row", null, td.getValue("zwei", "english"));
    }

}
